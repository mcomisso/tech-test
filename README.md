# MyFirstSlideshow

This "app" is a rather simple one.
It's only one screen divided into two areas. The app should display images represented by their URL in an array of 4 elements
``` 
["https://c1.staticflickr.com/6/5615/15570202337_0e64f5046e_k.jpg",
 "https://c1.staticflickr.com/4/3169/2846544061_cb7c04b46f_b.jpg",
 "https://i.redd.it/d8q1wkgu1awy.jpg",
 "http://www.kapstadt.de/webcam.jpg"]
 
  // Images under Creative Commons 
  // Images attributed to (in order) https://www.flickr.com/photos/_torne/
  // https://www.flickr.com/photos/chrisyarzab/
  // https://www.reddit.com/user/lalien42/
  // http://www.kapstadt.de/
```