//
//  CacheControlTests.swift
//  MyFirstSlideshow
//
//  Created by Matteo Comisso on 27/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

import UIKit
import XCTest
@testable import MyFirstSlideshow

/** CacheControlTests Class

*/
class CacheControlHeadersTests: XCTestCase {

    let invalidHeaders: HTTPHeaders = ["Cache-Control": "max-age=0"]
    let validHeaders: HTTPHeaders   = [:]
    let emptyHeaders: HTTPHeaders   = [:]

    func testCacheControlHeader() {

        let nilControl = CacheControl(headers: invalidHeaders, status: HTTPResponseCode.ok.rawValue)
        XCTAssert(nilControl == nil, "CacheControl should be nil")

        let validControl = CacheControl(headers: validHeaders, status: HTTPResponseCode.ok.rawValue)
        XCTAssert(validControl != nil, "CacheControl should be valid")

    }

    func testEtagheader() {
        let header = ["Etag": "345678uhgfr43wer678i"]

        let cacheControl = CacheControl(headers: header, status: 200)
        XCTAssert(cacheControl != nil, "CacheControl should be valid")
    }

    func testExpiration() {
        let headers = ["Etag": "345678uhgfr43wer678i",
                       "Expires": "Thu, 31 Dec 2037 23:59:59 GMT"]

        let cacheControl = CacheControl(headers: headers, status: 200)
        XCTAssert((cacheControl!.expires != nil), "Expires should be set")
    }
}
