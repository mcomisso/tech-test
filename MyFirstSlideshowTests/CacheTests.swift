//
//  CacheTests.swift
//  MyFirstSlideshow
//
//  Created by Matteo Comisso on 29/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

import UIKit
import XCTest
@testable import MyFirstSlideshow


class CacheTests: XCTestCase {

    let lowValues = 2
    let highValues = 1000

    func testNumberOfItems() {
        let cache = MFSCache<MCImageCacheElement>.init(cacheLimit: lowValues)

        for value in 0..<10 {
            let resp = HTTPURLResponse(url: URL(string: "http://cacheElement.com/\(value)")!, statusCode: HTTPResponseCode.ok.rawValue, httpVersion: nil, headerFields: ["Cache-Control": "max-age=234567890"])


            let cacheElement = MCImageCacheElement(response: resp!, url: resp?.url)
            let key = (cacheElement?.key)! as NSString
            cache.setObject(cacheElement!, forKey: key)
        }

        XCTAssert(cache.cachedItems == lowValues)
    }

}
