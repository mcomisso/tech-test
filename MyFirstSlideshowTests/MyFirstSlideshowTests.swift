//
//  MyFirstSlideshowTests.swift
//  MyFirstSlideshowTests
//
//  Created by Charles Vu on 17/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

import XCTest
@testable import MyFirstSlideshow

class ResponseCodeTests: XCTestCase {

    let validStatusCodes = HTTPResponseCode.validCodes()
    let headers = ["Cache-Control": "max-age=100, public"]

    func testValidCodes() {

        for statusCode in validStatusCodes {
            let cacheControl = CacheControl(headers: headers, status: statusCode.rawValue)
            XCTAssert(cacheControl != nil, "CacheControl should be set")
        }
    }
}

