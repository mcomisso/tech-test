//
//  ImageCacheElements.swift
//  MyFirstSlideshow
//
//  Created by Matteo Comisso on 29/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

import UIKit
import XCTest
@testable import MyFirstSlideshow

class MCImageCacheElementTests: XCTestCase {

    let normalResponse = HTTPURLResponse(url: URL(string: "http://cacheElement.com")!, statusCode: HTTPResponseCode.ok.rawValue, httpVersion: nil, headerFields: ["Cache-Control": "max-age=234567890"])

    let expiresResponse = HTTPURLResponse(url: URL(string: "http://cacheElement.com")!, statusCode: HTTPResponseCode.ok.rawValue, httpVersion: nil, headerFields: ["Expires": "Thu, 31 Dec 2016 23:59:59 GMT"])

    let invalidResponse = HTTPURLResponse(url: URL(string: "http://cacheElement.com")!, statusCode: HTTPResponseCode.ok.rawValue, httpVersion: nil, headerFields: ["Cache-Control": "no-store"])

    let expiredResponse = HTTPURLResponse(url: URL(string: "http://cacheElement.com")!, statusCode: HTTPResponseCode.ok.rawValue, httpVersion: nil, headerFields: ["Cache-Control": "max-age=0"])

    func testResponseExpiration() {
        let cacheElement = MCImageCacheElement(response: normalResponse!, url: normalResponse?.url)
        XCTAssert(cacheElement?.isExpired == false)

        let expiredElement = MCImageCacheElement(response: expiresResponse!, url: expiresResponse?.url)
        XCTAssert(expiredElement?.isExpired == true)
    }

    func testNotCacheable() {
        let invalidCache = MCImageCacheElement(response: invalidResponse!, url: normalResponse?.url)
        XCTAssert(invalidCache == nil)

        let expiredCache = MCImageCacheElement(response: expiredResponse!, url: normalResponse?.url)
        XCTAssert(expiredCache == nil)
    }
}
