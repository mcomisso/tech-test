//
//  MFSConstants.swift
//  MyFirstSlideshow
//
//  Created by Matteo Comisso on 25/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

/*
 Discussion: A place to save the base settings of classes. 
 */

import Foundation

struct MFSConstants {
    struct Settings {
        static let cacheMaxElements = 10000
    }
}
