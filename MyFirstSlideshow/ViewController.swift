//
//  ViewController.swift
//  MyFirstSlideshow
//
//  Created by Charles Vu on 17/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

/*
 
 Discussion: The UI is made only with code because seems the safer thing when working on git with other developers.
 
 The collectionView is added to the viewController's view, and uses a custom cell with inside the custom slideshow.
 
 Datasource values are just for testing multiple instances/collectionView behaviour.

 */

import UIKit

class ViewController: UIViewController, NSURLConnectionDataDelegate {
    // Images under Creative Commons
    // Images attributed to (in order) https://www.flickr.com/photos/_torne/
    // https://www.flickr.com/photos/chrisyarzab/
    // https://www.reddit.com/user/lalien42/
    // http://www.kapstadt.de/

    let images: Array<String> = ["https://c1.staticflickr.com/6/5615/15570202337_0e64f5046e_k.jpg",
                                 "https://c1.staticflickr.com/4/3169/2846544061_cb7c04b46f_b.jpg",
                                 "https://i.redd.it/d8q1wkgu1awy.jpg",
                                 "http://www.kapstadt.de/webcam.jpg"]


    lazy var collectionView: UICollectionView! = { [weak self] in
        guard let strongSelf = self else { fatalError() }
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: strongSelf.view.frame.size.width, height: 300)
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0

        let collectionV = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionV.register(MFSlideshowCell.self, forCellWithReuseIdentifier: "ReuseIdentifier")
        collectionV.translatesAutoresizingMaskIntoConstraints = false
        collectionV.backgroundColor = .white
        collectionV.dataSource = strongSelf

        return collectionV
    }()


    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        self.view.subviews.forEach {
            $0.isHidden = true
        }

        self.view.addSubview(self.collectionView)

        NSLayoutConstraint.activate([self.collectionView.topAnchor.constraint(equalTo: self.view.topAnchor),
                                     self.collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
                                     self.collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
                                     self.collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)])

    }
}

extension ViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReuseIdentifier", for: indexPath) as? MFSlideshowCell {
            let slideshow = MFSSlideshowView(frame: .zero, images: self.images)
            cell.setSlideshow(slideshow: slideshow)
            cell.setNeedsLayout()
            cell.layoutIfNeeded()

            return cell
        }
        fatalError()
    }

}
