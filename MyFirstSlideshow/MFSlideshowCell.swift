//
//  MFSlideshowCell.swift
//  MyFirstSlideshow
//
//  Created by Matteo Comisso on 26/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

/*
    Discussion: This cell contains a custom slideshow inside its own contentView.
 */

import UIKit
class MFSlideshowCell: UICollectionViewCell {


    override func layoutIfNeeded() {
        super.layoutIfNeeded()

        NSLayoutConstraint.activate([(self.slideshow?.topAnchor.constraint(equalTo: self.contentView.topAnchor))!,
                                     (self.slideshow?.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor))!,
                                     (self.slideshow?.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor))!,
                                     (self.slideshow?.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor))!])
    }

    var slideshow: MFSSlideshowView? = nil


    func setSlideshow(slideshow: MFSSlideshowView) {
        self.slideshow = slideshow
        self.slideshow?.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(self.slideshow!)
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.slideshow?.removeFromSuperview()
        self.slideshow = nil
    }
}
