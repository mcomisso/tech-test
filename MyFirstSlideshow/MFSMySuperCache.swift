//
//  MFSMySuperCache.swift
//  MyFirstSlideshow
//
//  Created by Matteo Comisso on 25/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

import Foundation
import UIKit

protocol MySuperCache {
    func get(imageAtURLString imageURLString: String, completionBlock: (UIImage?) -> Void)
}


extension MFSCache: MySuperCache {
    func get(imageAtURLString imageURLString: String, completionBlock: (UIImage?) -> Void) {

        if let element = self.object(forKey: imageURLString as NSString) as? MCImageCacheElement {

            if let image = element.image {
                completionBlock(image)
                return
            }
        }
        completionBlock(nil)
    }
}
