//
//  MFSImageDownloader.swift
//  MyFirstSlideshow
//
//  Created by Matteo Comisso on 24/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

import Foundation
import UIKit


/*
 Discussion: The MFSImageDownloader takes the ownership to download images. 
 The usage goes through the "downloadEventually" method, that tries to load the image from the network only if needed, otherwise it will return the cached element, if any.

 To download a image, maximum of two calls are made: One with only a HEAD HTTP mehtod to determine if there was any change on etag and one to actual download it if needed.

 Completion blocks are called within a DispatchQueue.main.async method, so the receiver (closer to UI) does not need to call it.

 */

class MFSImageDownloader<T: Cacheable> {

    private let urlSession = URLSession(configuration: .default)

    private let cache: MFSCache<T>

    init(with cache: MFSCache<T>) {
        self.cache = cache
    }

    func downloadEventually(url: URL, completion: @escaping (UIImage?) -> Void) {

        var request = URLRequest(url: url)

        let cachedData = self.cache.object(forKey: (url.absoluteString as NSString)) as? MCImageCacheElement

        // If cachedData is not expired, and there is a etag set, request the headers to make sure there is the need to update the resource
        if cachedData?.isExpired == false,
            let etag = cachedData?.cacheControl.etag {
            request.httpMethod = "HEAD"
            request.addValue(etag, forHTTPHeaderField: "If-None-Match")
        }

        let downloadTask = self.urlSession.downloadTask(with: request) { (data, resp, err) in
            if let error = err as? URLError {

                // Might be safer to add more error codes
                if [.notConnectedToInternet, .timedOut].contains(error.code) {
                    print("not connected")
                    DispatchQueue.main.async {
                        completion(cachedData?.image)
                    }
                }

            } else if let response = resp as? HTTPURLResponse {

                // If there was a previous cache and resource was modified
                if cachedData?.cacheControl.etag != nil && response.statusCode != HTTPResponseCode.notModified.rawValue {

                    // Change value
                    self.downloadImage(url: url, completion: completion)

                } else {
                    // return the image if there is any
                    if let image = cachedData?.image {

                        DispatchQueue.main.async {
                            completion(image)
                        }
                    } else {
                        self.downloadImage(url: url, completion: completion)
                    }
                }
            }
        }
        downloadTask.resume()
    }



    func downloadImage(url: URL, completion: @escaping (UIImage?) -> Void) {

        let request = URLRequest(url: url)

        let downloadTask = urlSession.downloadTask(with: request) { (url: URL?, response: URLResponse?, error: Error?) in
            if let error = error {
                print(error.localizedDescription)
                DispatchQueue.main.async {
                    completion(nil)
                }
            } else {

                if let response = response as? HTTPURLResponse,
                    let cacheElement = MCImageCacheElement(response: response, url: url),
                    let url = response.url?.absoluteString {

                    let cacheControl = response.cacheControl
                    print(cacheControl.debugDescription)

                    imageCache.setObject(cacheElement, forKey: url as NSString)
                }

                if let location = url {

                    do {
                        let data = try Data(contentsOf: location)

                        DispatchQueue.main.async {
                            completion(UIImage(data: data))
                        }

                    } catch {
                        DispatchQueue.main.async {
                            completion(nil)
                        }
                    }
                }
            }
        }
        
        downloadTask.resume()
    }
}
