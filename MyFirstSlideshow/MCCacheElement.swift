//
//  MCImageCacheElement.swift
//  MyFirstSlideshow
//
//  Created by Matteo Comisso on 25/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

import Foundation
import UIKit

/*
 Discussion: This class will be saved to disk with a NSCoding implementation. 
 Represents the actual cache element and its data (in this case a UIImage). Other types can be implemented by extending NSCoding and Cacheable protocols.
 */

final class MCImageCacheElement: NSObject, NSCoding, Cacheable {

    let urlString: String
    let cacheControl: CacheControl!
    let image: UIImage?

    var isExpired: Bool {
        get {
            if let expires = self.cacheControl.expires {
                if expires.timeIntervalSinceNow.isLessThanOrEqualTo(0.0) {
                    return true
                }
            }

            if let maxAge = self.cacheControl.maxAge {
                if maxAge.timeIntervalSinceNow.isLessThanOrEqualTo(0.0) {
                    return true
                }
            }

            return false
        }
    }

    var key: String {
        get {
            return urlString
        }
    }

    init?(response: HTTPURLResponse, url: URL?) {
        guard let cControl = response.cacheControl,
        let urlString = response.url?.absoluteString else { return nil }
        self.cacheControl = cControl
        self.urlString = urlString

        if let url = url,
            let data = try? Data(contentsOf: url),
            let image = UIImage(data: data) {
            self.image = image
        } else {
            self.image = nil
        }
    }

    required init?(coder aDecoder: NSCoder) {
        guard let urlString = aDecoder.decodeObject(forKey: "urlString") as? String,
         let cacheControlData = aDecoder.decodeObject(forKey: "cacheControl") as? StandardDict,
         let imageData = aDecoder.decodeObject(forKey: "image") as? Data else { return nil }

        self.urlString = urlString
        self.cacheControl = CacheControl(data: cacheControlData)
        self.image = UIImage(data:imageData)
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.urlString, forKey: "urlString")
        aCoder.encode(self.cacheControl.asDictionary, forKey: "cacheControl")

        if let image = image  {
            aCoder.encode(UIImagePNGRepresentation(image), forKey: "image")
        }

    }

}
