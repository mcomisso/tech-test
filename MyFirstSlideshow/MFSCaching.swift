//
//  MFSImageCaching.swift
//  MyFirstSlideshow
//
//  Created by Matteo Comisso on 24/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

/*
 MFSCache is the implementation of a disk-based cache. 
 It could be improved with a mid-layer NSCache component, to have a faster in-memory cache layer.
 */

import Foundation
import UIKit

typealias StandardDict = [String: Any]

protocol Cacheable {
    var key: String { get }
    var isExpired: Bool { get }
}

fileprivate typealias Path = String

// public instance of cache
let imageCache = MFSCache<MCImageCacheElement>()

final class MFSCache<T: Cacheable> {

    private let countLimit: Int!
    private let cacheDirectory: String = (NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first! as NSString).appendingPathComponent("MFSCache")

    public var cachedItems: Int {
        get {
            do {
                return try FileManager.default.contentsOfDirectory(atPath: self.cacheDirectory).count
            } catch {
                fatalError("Cache directory not found")
            }
        }
    }

    // MARK: Initialization

    init() {
        self.countLimit = MFSConstants.Settings.cacheMaxElements
        self.sharedInitialization()
    }

    public init(cacheLimit: Int) {
        self.countLimit = cacheLimit
        self.sharedInitialization()
    }

    private func sharedInitialization() {
        try? FileManager.default.createDirectory(atPath: self.cacheDirectory, withIntermediateDirectories: true, attributes: nil)
    }


    // Saves a new object to a saved folder (or updates the modification date to keep it fresh)
    private func saveToCacheFolder(_ obj: T, forKey key: NSString) {

        let path = self.pathFromKey(key: key as String)
        if NSKeyedArchiver.archiveRootObject(obj, toFile: path) {
            print("Wrote file")
        } else {
            print("Error while writing file")
        }
    }


    /// Removes the oldes file saved at the cache location
    private func removeOldestFile() {
        do {

            guard let path = URL(string: self.cacheDirectory) else { fatalError() }

            let contents = try FileManager.default.contentsOfDirectory(at: path, includingPropertiesForKeys: [.contentModificationDateKey], options: .skipsHiddenFiles)

            let oldest = contents.map { ($0, (try? $0.resourceValues(forKeys: [.contentModificationDateKey]))?.contentModificationDate ?? Date.distantPast) }.sorted(by: { $0.1 < $1.1 }).first

            if let oldest = oldest {
                try FileManager.default.removeItem(at: oldest.0)
            }
        } catch {
            print("Failure in \(#function)")
        }
    }


    fileprivate func pathFromKey(key: String) -> String {
        let key = key.replacingOccurrences(of: "/", with: "")
        return (self.cacheDirectory as NSString).appendingPathComponent(key as String)
    }


    func setObject(_ obj: T, forKey key: NSString) {

        // Countlimit-1 because the current file will be saved
        while self.cachedItems >= self.countLimit {
            self.removeOldestFile()
        }

        self.saveToCacheFolder(obj, forKey: key)
    }


    func object(forKey key: NSString) -> T? {

        let path = self.pathFromKey(key: key as String)

        if let obj = NSKeyedUnarchiver.unarchiveObject(withFile: path) as? T {
            if obj.isExpired {
                return nil
            }
            return obj
        }
        return nil
    }
}


extension MFSCache {
    // UTILITIES

    public func refreshModificationDate(cache: T) {
        do {
            let path = self.pathFromKey(key: cache.key)
            try FileManager.default.setAttributes([FileAttributeKey.modificationDate: Date()], ofItemAtPath: path)
        } catch {
            print("Error while resetting modification date")
        }
    }
}
