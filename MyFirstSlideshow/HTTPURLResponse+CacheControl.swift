//
//  HTTPURLResponse+CacheControl.swift
//  MyFirstSlideshow
//
//  Created by Matteo Comisso on 28/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

/*
 Discussion: This HTTPURLResponse extension connects the cacheControl to a HTTPURLResponse.
 This comes handy to have a CacheControl value (representing Cache-Control and related headers) available as property.
 */

import Foundation

extension HTTPURLResponse {

    var cacheControl: CacheControl? {
        get {
            return CacheControl(headers: self.allHeaderFields, status: self.statusCode)
        }
    }
}
