//
//  MCCacheControl.swift
//  MyFirstSlideshow
//
//  Created by Matteo Comisso on 25/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

/*
 Discussion: this file contains HTTP related implementations. HTTP codes, DateFormatters, CacheControl structs, etc.
 */

import Foundation


typealias HTTPHeaders = [AnyHashable: Any]

typealias ETag = String

enum CachePrivacy: String {
    case `public`, `private`
}

enum Directive: String {
    case noCache = "no-cache", noStore = "no-store", mustRevalidate = "must-revalidate"
}

/// A date formatter that follows these implementations
///
/// - RFC1123: RFC1123 Dates
/// - ANSIC: ANSIC Dates
/// - RFC850: RFC850 Dates
enum HTTPDateFormatter: String {
    case RFC1123 = "EEE, dd MMM yyyy HH:mm:ss z"
    case ANSIC = "EEE MMM d HH:mm:ss yyyy"
    case RFC850 = "EEEE, dd-MMM-yy HH:mm:ss z"

    func dateFromString(string: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = self.rawValue
        return dateFormatter.date(from: string)
    }
}

/*
 Cacheable responses

 200 OK
 203 Non-Authoritative Information
 204 No Content
 206 Partial Content

 300 Multiple Choices
 301 Moved Permanently

 404 Not Found
 405 Method Not Allowed
 410 Gone
 414 URI Too Long

 501 Not Implemented
 */

enum HTTPResponseCode: Int {
    case ok = 200, created, accepted, nonAuthorativeInformation, noContent, partialContent
    case multipleChoice = 300, movedPermanently, found, seeOther, notModified
    case notFound = 404, methodNotAllowed
    case gone = 410
    case uriTooLong = 414

    case notImplemented = 501

    static func validCodes() -> [HTTPResponseCode] {

        var validCodes: [HTTPResponseCode] = []

        // 2xx codes
        validCodes.append(contentsOf: [.ok, .nonAuthorativeInformation, .noContent, .partialContent])

        // 3xx codes
        validCodes.append(contentsOf: [.multipleChoice, .movedPermanently])

        // 4xx codes
        validCodes.append(contentsOf: [.notFound, .methodNotAllowed, .gone, .uriTooLong])

        // 5xx codes
        validCodes.append(.notImplemented)
        
        return validCodes
    }
}



/// This struct represents the `Cache-Control`, `Etag` and `Expires` header of a HTTPURLresponse.
struct CacheControl {

    // Etag header
    var etag: String?

    // Cache control header
    var maxAge: Date?
    var privacy: CachePrivacy?

    var directives: [Directive] = []

    // Expires header
    var expires: Date?


    var asDictionary: StandardDict {
        get {

            var retVal: StandardDict = [:]

            if let etag = self.etag {
                retVal["etag"] = etag
            }

            if let maxAge = self.maxAge {
                retVal["maxAge"] = maxAge
            }

            if let privacy = self.privacy?.rawValue {
                retVal["privacy"] = privacy
            }

            if let expires = self.expires {
                retVal["expires"] = expires
            }

            retVal["directives"] = self.directives

            return retVal
        }
    }

    init?(data: StandardDict) {

        self.etag = data["etag"] as? String
        self.maxAge = data["maxAge"] as? Date
        self.expires = data["expires"] as? Date

        if let directives = data["directives"] as? [Directive] {
            self.directives = directives
        }

        if let privacy = data["privacy"] as? String {
            self.privacy = CachePrivacy(rawValue: privacy)
        }
    }


    init?(headers: HTTPHeaders, status: Int) {

        let validStatusCodes = HTTPResponseCode.validCodes()

        if let responseCode = HTTPResponseCode(rawValue: status) {
            if (validStatusCodes.contains(responseCode) == false) {
                // Uncacheable response
                return nil
            }
        } else {
            return nil
        }

        if let cacheControlString = headers["Cache-Control"] as? String {
            let components = Set(cacheControlString.replacingOccurrences(of: " ", with: "").components(separatedBy: ","))

            for component in components {
                if component.contains("max-age") {
                    // max age
                    if let maxAgeString = component.components(separatedBy: "=").last,
                        let maxAgeValue = Int(maxAgeString) {

                        if maxAgeValue == 0 {
                            return nil
                        }

                        if maxAgeString.isEmpty {
                            return nil
                        }

                        self.maxAge = Date().addingTimeInterval(Double(maxAgeValue))
                    }
                } else if let privacy = CachePrivacy(rawValue: component) {
                    self.privacy = privacy
                } else if let directive = Directive(rawValue: component) {
                    if [.noCache, .noStore, .mustRevalidate].contains(directive) {
                        // Exit if no cache directive detected
                        return nil
                    }
                    self.directives.append(directive)
                }
            }
        }

        if let expires = headers["Expires"] as? String {
            if let date = HTTPDateFormatter.ANSIC.dateFromString(string: expires) {
                self.expires = date
            } else if let date = HTTPDateFormatter.RFC1123.dateFromString(string: expires) {
                self.expires = date
            } else if let date = HTTPDateFormatter.RFC850.dateFromString(string: expires) {
                self.expires = date
            }
        }

        if let etag = headers["Etag"] as? String {
            self.etag = etag
        }
    }
}
