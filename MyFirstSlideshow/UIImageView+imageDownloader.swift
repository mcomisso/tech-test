//
//  UIImageView+imageDownloader.swift
//  MyFirstSlideshow
//
//  Created by Matteo Comisso on 25/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {

    func setImageFromURL(url: URL) {

        let imageDownloader = MFSImageDownloader(with: imageCache)

        imageDownloader.downloadEventually(url: url) { (image) in
            if let image = image {
                self.image = image
            } else {
                // Manage errors
            }
        }
    }
}
