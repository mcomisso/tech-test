//
//  MFSlideshow.swift
//  MyFirstSlideshow
//
//  Created by Matteo Comisso on 24/05/2017.
//  Copyright © 2017 Yoti. All rights reserved.
//

import Foundation
import UIKit


final class MFSSlideshowView: UIView {

    // MARK: Private properties

    fileprivate var imagesURLs: [URL] = [] {
        didSet {

            // When the imagesURL is set, reload the indicators view

            while imagesURLs.count != self.indicator.arrangedSubviews.count {

                if imagesURLs.count < self.indicator.arrangedSubviews.count {

                    if let view = self.indicator.arrangedSubviews.first {
                        self.indicator.removeArrangedSubview(view)
                    }

                } else if imagesURLs.count > self.indicator.arrangedSubviews.count {

                    let led = UIView()
                    led.backgroundColor = .lightGray

                    self.indicator.addArrangedSubview(led)

                }
            }

            self.loadNextImage()
        }
    }

    private var currentDisplayedImage: URL? = nil

    lazy private var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false

        return imageView
    }()

    lazy private var indicator: UIStackView = {
        let stackView = UIStackView()
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false

        return stackView
    }()

    lazy private var actionButton: UIButton = { [weak self] in
        guard let strongSelf = self else { fatalError() }
        let button = UIButton(type: UIButtonType.custom)
        button.setTitle("Download !", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.addTarget(strongSelf, action: #selector(strongSelf.onButtonClicked(_:)), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false

        return button
    }()

    // MARK: Initializers

    init(frame: CGRect, images: [String]) {

        let urls: [URL] = images.map {
            guard let url = URL(string: $0) else { fatalError("Cannot initialize selected string as URL \($0)") }
            return url
        }
        super.init(frame: frame)

        self.setImageUrls(urls)

        self.translatesAutoresizingMaskIntoConstraints = false

        self.addSubview(self.imageView)
        self.addSubview(self.indicator)
        self.addSubview(self.actionButton)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        NSLayoutConstraint.activate([self.imageView.topAnchor.constraint(equalTo: self.topAnchor),
                                     self.imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                                     self.imageView.trailingAnchor.constraint(equalTo: self.trailingAnchor)])

        NSLayoutConstraint.activate([self.indicator.heightAnchor.constraint(equalToConstant: 10),
                                     self.indicator.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                                     self.indicator.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                                     self.indicator.topAnchor.constraint(equalTo: self.imageView.bottomAnchor)])

        NSLayoutConstraint.activate([self.actionButton.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                                     self.actionButton.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                                     self.actionButton.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                                     self.actionButton.topAnchor.constraint(equalTo: self.indicator.bottomAnchor)])

    }
    
    required init?(coder aDecoder: NSCoder) {
        // Todo: implement with this method to support storyboards.
        fatalError("init(coder:) has not been implemented")
    }

    // The private func triggers didSet even on initializers.
    private func setImageUrls(_ urls: [URL]) {
        self.imagesURLs = urls
    }

    fileprivate func loadNextImage() {

        var url: URL!

        if let currentImage = self.currentDisplayedImage,
            let index = self.imagesURLs.index(of: currentImage) {

            let nextIndex = index + 1
            url = self.imagesURLs[nextIndex % self.imagesURLs.count]
        } else {
            if let firstUrl = self.imagesURLs.first {
                url = firstUrl
            }
        }

        self.currentDisplayedImage = url
        self.imageView.setImageFromURL(url: url)
        self.setIndicatorForUrl(url: url)
    }

    private func setIndicatorForUrl(url: URL) {

        self.indicator.arrangedSubviews.forEach { $0.backgroundColor = .lightGray }

        if let index = self.imagesURLs.index(of: url) {
            let view = self.indicator.arrangedSubviews[index]
            view.backgroundColor = .green
        }
    }
}

fileprivate typealias MFSSlideshowViewPublicMethods = MFSSlideshowView
extension MFSSlideshowViewPublicMethods {

    // MARK: Manipulation of urls in custom view

    func removeImage(with url: URL) {
        if let index = self.imagesURLs.index(of: url) {
            self.imagesURLs.remove(at: index)
        }
    }

    func addImage(imageUrlString: String) {
        guard let url = URL(string: imageUrlString) else { return }

        self.imagesURLs.append(url)
    }

    // MARK: Target-Action button

    func onButtonClicked(_ sender: UIButton) {
        self.loadNextImage()
    }

}
